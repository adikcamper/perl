# perl
Something in perl! Be carefull... It is something ;)

# Example of OneLiners

```perl
cd /var/log

cat syslog | perl -e '$count=0; while ($line = <STDIN>){$count++; print("$count $line");}'

cat syslog | perl -e '$count=0; while ($line = <STDIN>){if ($line =~ m/network/gi) {$count++; print("$count $line");}else{print"ELSE\n"}}'

cat syslog | perl -e '$count=0; while($line = <STDIN>){if ($line =~ m/network/i){$count++}}print("$count\n");'

cat syslog | perl -ne 'm/(\d+\.\d+\.\d+\.\d+)/ && print ("$1\n")'

```
