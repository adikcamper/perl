#!/usr/bin/perl


use strict;
use warnings;

sub main {
    open(FH, "<", "/usr/share/dict/polish");
    my $counter = 0;
    my $matchCounter = 0;
    foreach my $i (<FH>){
    	## works:
        #print $i if ( $i =~ m/da$/i );
        
        if ( $i =~ m/da$/i ){
        	$matchCounter++;
        }
        $counter++;
    }
    
    print("\$matchCounter: $matchCounter\n");
    print("\$counter: $counter\n");
    
    close(FH);
}

main
